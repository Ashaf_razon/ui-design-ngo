package com.avis.avistechbd.uidesign.customview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.avis.avistechbd.uidesign.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class Mainpage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainpage);
        ButterKnife.bind(this);
        getSupportActionBar().hide();
    }

    @OnClick(R.id.u_sign_in)
    public void onUSignInClicked() {
        Intent signInAuth = new Intent(Mainpage.this, SignInAct.class);
        startActivity(signInAuth);
    }

    @OnClick(R.id.u_sign_up)
    public void onUSignUpClicked() {
        Intent getPhonAuth = new Intent(Mainpage.this, PhoneAuth.class);
        startActivity(getPhonAuth);
    }
}
