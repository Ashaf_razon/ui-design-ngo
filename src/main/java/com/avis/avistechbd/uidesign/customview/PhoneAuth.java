package com.avis.avistechbd.uidesign.customview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.avis.avistechbd.uidesign.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhoneAuth extends AppCompatActivity {

    @BindView(R.id.enterPhoneAuth)
    EditText enterPhoneAuth;
    @BindView(R.id.codeSendInPhnText)
    TextView codeSendInPhnText;
    @BindView(R.id.getAuthCode_1)
    EditText getAuthCode1;
    @BindView(R.id.getAuthCode_2)
    EditText getAuthCode2;
    @BindView(R.id.getAuthCode_3)
    EditText getAuthCode3;
    @BindView(R.id.getAuthCode_4)
    EditText getAuthCode4;
    @BindView(R.id.getAuthCode_5)
    EditText getAuthCode5;
    @BindView(R.id.getAuthCode_6)
    EditText getAuthCode6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_auth);
        getSupportActionBar().hide();
        ButterKnife.bind(this);
    }

    @OnClick({R.id.varifyPhonAuth, R.id.submit_code})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.varifyPhonAuth:
                useThread();
                break;
            case R.id.submit_code:
                Intent createAcc = new Intent(PhoneAuth.this, CreateUserAcc.class);
                startActivity(createAcc);
                break;
        }
    }

    private void useThread() {

    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }
}
