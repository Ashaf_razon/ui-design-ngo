package com.avis.avistechbd.uidesign.customview;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.TextView;
import com.avis.avistechbd.uidesign.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInAct extends AppCompatActivity {

    @BindView(R.id.getSignedUphone)
    EditText getSignedUphone;
    @BindView(R.id.getSignedUpass)
    EditText getSignedUpass;
    @BindView(R.id.forgoUpass)
    TextView forgoUpass;
    @BindView(R.id.createNewUacc)
    TextView createNewUacc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        getSupportActionBar().hide();
        ButterKnife.bind(this);
        forgoUpass.setPaintFlags(forgoUpass.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        createNewUacc.setPaintFlags(createNewUacc.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
    }

    @OnClick(R.id.getUSignedSubmit)
    public void onViewClicked() {

    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }
}
