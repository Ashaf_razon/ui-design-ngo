package com.avis.avistechbd.uidesign.customview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.avis.avistechbd.uidesign.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateUserAcc extends AppCompatActivity {

    @BindView(R.id.getUname)
    EditText getUname;
    @BindView(R.id.radioSex)
    RadioGroup radioSexGroup;
    RadioButton radioButton;
    @BindView(R.id.getUpass)
    EditText getUpass;
    @BindView(R.id.getUconfPass)
    EditText getUconfPass;
    @BindView(R.id.createUacc)
    Button createUacc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user_acc);
        getSupportActionBar().hide();
        ButterKnife.bind(this);
    }

    @OnClick(R.id.createUacc)
    public void onViewClicked() {
    }

    private String getRadioText() {
        String radioText = null;
        try{
            int selectedGender = radioSexGroup.getCheckedRadioButtonId();
            radioButton = (RadioButton) findViewById(selectedGender);
            radioText = radioButton.getText().toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return radioText;
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }
}
